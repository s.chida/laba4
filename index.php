<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages = array();

    if (!empty($_COOKIE['save'])) {
        setcookie('save', '', 100000);
        $messages[] = 'Спасибо, результаты сохранены.';
    }

    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['birthyear'] = !empty($_COOKIE['birthyear_error']);
    $errors['radio1'] = !empty($_COOKIE['radio1_error']);
    $errors['radio2'] = !empty($_COOKIE['radio2_error']);
    $errors['bio'] = !empty($_COOKIE['bio_error']);
    $errors['checkbox'] = !empty($_COOKIE['checkbox_error']);
    $errors['super'] = !empty($_COOKIE['super_error']);


    if ($errors['fio']) {
        setcookie('fio_error', '', 100000);
        $messages[] = '<div class="error">Пусто или вы ввели недопустимые символы в поле имя!</div>';
    }

    if ($errors['email']) {
         setcookie('email_error', '', 100000);
         $messages[] = '<div class="error">Пусто или вы ввели недопустимые символы в поле email!</div>';
     }
     if ($errors['birthyear']) {
         setcookie('birthyear_error', '', 100000);
         $messages[] = '<div class="error">Пусто или вы еще не родились!</div>';
     }

     if ($errors['radio1']) {
         setcookie('radio1_error', '', 100000);
         $messages[] = '<div class="error">Вы не выбрали пол!</div>';
     }

     if ($errors['radio2']) {
         setcookie('radio2_error', '', 100000);
         $messages[] = '<div class="error">Вы не выбрали кол-во конечностей!</div>';
     }

     if ($errors['bio']) {
         setcookie('bio_error', '', 100000);
         $messages[] = '<div class="error">Вы не заполнили биографию!</div>';
     }

    if ($errors['checkbox']) {
        setcookie('checkbox_error', '', 100000);
        $messages[] = '<div class="error">Вы не отметили согласие на обработку!</div>';
    }
    if ($errors['super']) {
        setcookie('super_error', '', 100000);
        $messages[] = '<div class="error">Вы не выбрали способности!</div>';
    }


    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['birthyear'] = empty($_COOKIE['birthyear_value']) ? '' : $_COOKIE['birthyear_value'];
    $values['radio1'] = empty($_COOKIE['radio1_value']) ? '' : $_COOKIE['radio1_value'];
    $values['radio2'] = empty($_COOKIE['radio2_value']) ? '' : $_COOKIE['radio2_value'];
    $values['bio'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];
    $values['checkbox'] = empty($_COOKIE['checkbox_value']) ? '' : $_COOKIE['checkbox_value'];
    $values['super'] = empty($_COOKIE['super_value']) ? '' : $_COOKIE['super_value'];
    include('form.php');
}
else {
    $errors = FALSE;
    if (preg_match("/[^(\w)|(\x7F-\xFF)|(\s)]/",$_POST['fio']) || empty($_POST['fio'])) {
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }

    else {
        setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }

    if (preg_match("/[^(\w)|(\@)|(\.)|(\-)]/",$_POST['email'])  || empty($_POST['email'])) {
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }

    else {
        setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
    }

    if (($_POST['birthyear']>2021) || empty($_POST['birthyear'])) {
        setcookie('birthyear_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }

    else {
        setcookie('birthyear_value', $_POST['birthyear'], time() + 30 * 24 * 60 * 60);
    }

    if ( empty($_POST['radio1'])) {
        setcookie('radio1_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }

    else {
        setcookie('radio1_value', $_POST['radio1'], time() + 30 * 24 * 60 * 60);
    }

    if ( empty($_POST['radio2'])) {
        setcookie('radio2_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }

    else {
        setcookie('radio2_value', $_POST['radio2'], time() + 30 * 24 * 60 * 60);
    }

    if ( empty($_POST['bio'])) {
        setcookie('bio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }

    else {
        setcookie('bio_value', $_POST['bio'], time() + 30 * 24 * 60 * 60);
    }

    if ( empty($_POST['checkbox'])) {
        setcookie('checkbox_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }

    else {
        setcookie('checkbox_value', $_POST['checkbox'], time() + 30 * 24 * 60 * 60);
    }

    $myselect = $_POST['super'];
    $flag=0;
    if(empty($myselect))
    {
        setcookie('super_error','1',time() + 24*60*60);
        $errors=TRUE;
        $flag=1;
    }

    if (!empty($myselect)) {
        foreach ($myselect as $ability) {
            if (!is_numeric($ability)) {
                setcookie('super_error', '2', time() + 24 * 60 * 60);
                $errors = TRUE;
                $flag=1;
                break;
            }

        }
    }
    if($flag==0) {
        setcookie('super_value', $_POST['super'], time() + 30 * 24 * 60 * 60);
    }

    if ($errors) {
        header('Location: index.php');
        exit();
    }
    else {
        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('birthyear_error', '', 100000);
        setcookie('radio1_error', '', 100000);
        setcookie('radio2_error', '', 100000);
        setcookie('bio_error', '', 100000);
        setcookie('checkbox_error', '', 100000);
        setcookie('super_error', '', 100000);

    }

    $user = 'u20990';
    $pass = '7645415';
    $db = new PDO('mysql:host=localhost;dbname=u20990', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

    $stmt = $db->prepare("INSERT INTO form (name,year,email,sex,limb,bio,checkbox) VALUES (:fio,:birthyear,:email, :sex,:limb,:bio,:checkbox )");
    $stmt -> execute(array('fio'=>$_POST['fio'],'birthyear'=>$_POST['birthyear'],'email'=>$_POST['email'],'sex'=>$_POST['radio1'],'limb'=>$_POST['radio2'],'bio'=>$_POST['bio'],'checkbox'=>$_POST['checkbox']));

    $form_id =  $db->lastInsertId();
    $myselect = $_POST['super'];
    if (!empty($myselect)) {
        foreach ($myselect as $ability) {
            if (!is_numeric($ability)) {
                continue;
            }
            $stmt = $db->prepare("INSERT INTO ability (form_id, ability_id) VALUES (:form_id, :ability_id)");
            $stmt -> execute(array(
                'form_id' => $form_id,
                'ability_id' => $ability
            ));
        }
    }

    setcookie('save', '1');
    header('Location: index.php');
}

